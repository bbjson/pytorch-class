nn.LSTM
LSTM 类描述

torch.nn.LSTM 是 PyTorch 中用于构建长短期记忆（LSTM）网络的类。LSTM 是一种特殊类型的循环神经网络（RNN），特别适合处理和预测时间序列数据中的长期依赖关系。
LSTM 类的功能和作用

    长短期记忆: LSTM 通过引入“门”机制（包括输入门、遗忘门、输出门）来控制信息的保留和遗忘，这有助于解决传统 RNN 在长序列上训练时的梯度消失问题。

LSTM 类的参数

    input_size: 输入特征的数量。
    hidden_size: 隐藏状态的特征数量。
    num_layers: RNN层的数量。例如，num_layers=2 表示两个 LSTM 层堆叠在一起。
    bias: 如果为 False，则层不使用偏置权重 b_ih 和 b_hh。
    batch_first: 如果为 True，则输入和输出张量的格式为 (batch, seq, feature)；否则为 (seq, batch, feature)。
    dropout: 如果非零，将在每个 LSTM 层的输出上引入一个 Dropout 层，除了最后一层。
    bidirectional: 如果为 True，则变为双向 LSTM。
    proj_size: 如果大于 0，则使用带有投影的 LSTM。这将改变 LSTM 单元的输出维度和某些权重矩阵的形状。

输入和输出

    输入: input, (h_0, c_0)
        input: 形状为 (seq_len, batch, input_size) 或 (batch, seq_len, input_size)（如果 batch_first=True）的张量，包含输入序列的特征。
        h_0: 形状为 (num_layers * num_directions, batch, hidden_size) 的张量，包含初始隐藏状态。
        c_0: 形状为 (num_layers * num_directions, batch, hidden_size) 的张量，包含初始细胞状态。

    输出: output, (h_n, c_n)
        output: 包含最后一层 LSTM 的输出特征（h_t）的张量。
        h_n: 形状为 (num_layers * num_directions, batch, hidden_size) 的张量，包含序列中每个元素的最终隐藏状态。
        c_n: 形状为 (num_layers * num_directions, batch, hidden_size) 的张量，包含序列中每个元素的最终细胞状态。

注意事项

    权重和偏置的初始化: 所有权重和偏置都从均匀分布 U(-k, k) 初始化，其中 k = 1 / hidden_size。
    双向 LSTM: 对于双向 LSTM，h_n 和 output 的含义有所不同。h_n 包含最终的前向和后向隐藏状态，而 output 包含每个时间步的隐藏状态。
    投影大小: proj_size 应小于 hidden_size。
    非确定性问题: 在某些版本的 cuDNN 和 CUDA 上，LSTM 函数可能存在非确定性问题。可以通过设置特定的环境变量来强制确定性行为。
