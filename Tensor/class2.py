
import torch

x = torch.randn(4,4)
print(x)
y = x.view(8,2)
print(y)
z = y.view(-1,16)
print(z)

w = z.reshape(2,8)
print(w)
w = z.reshape(-1,4)
print(w)


x = torch.rand(3,2,4,5)
print(torch.squeeze(x))
print(x.unsqueeze(4))

print("*"*80)

x = torch.ones(2,2,requires_grad=True)
y = x.sum()+4
y +=1
y.backward() # 反向传播,计算梯度
print(x.grad)

































































